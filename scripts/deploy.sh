#!/bin/bash

zip -r dist.zip dist
ssh app "mkdir -p ${DEPLOY_PATH}/webapp;"
scp dist.zip ${USER}@${HOSTNAME}:${DEPLOY_PATH}
rm -f dist.zip
ssh app "cd ${DEPLOY_PATH};unzip -o dist.zip;rm -f dist.zip;if [ -d webapp ];then mv webapp webapp.`date +%Y%m%d%H%M`;fi;mv dist webapp"
