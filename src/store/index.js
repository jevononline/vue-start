import Vue from 'vue'
import Vuex from 'vuex'
import About from './modules/about'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    about: About
  }
})
