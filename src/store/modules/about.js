
export default {
  namespaced: true,
  state: {
    count: 1,
  },
  mutations: {
    ADD_COUNT: (state, val) => {
      state.count += val;
    }
  },
  actions: {
    addCount({ commit }, val) {
      commit('ADD_COUNT', val);
    }
  }
};
